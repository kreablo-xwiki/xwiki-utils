{-# LANGUAGE OverloadedStrings #-}
module Main where

import Test.HUnit
import System.Exit (exitFailure, exitSuccess)
import System.IO
import Data.Conduit
import Data.Conduit.Combinators
import qualified Data.ByteString.Lazy as BL
import Data.Aeson (eitherDecode)
import Data.Text as T
import Data.Text.Lazy as LT
import Data.Text.Lazy.Builder

import XWiki.XClass
import XWiki.XObj
import XWiki.Model

tests = TestList [testParseAndSerializeObj, testParseXClass, testParseXClassJson, testXobj2formdata, testParseXobj]

assertServerClass (XClass id name props) = do
  assertEqual "id" id "XWiki.XWikiServerClass"
  assertEqual "name" name "XWiki.XWikiServerClass"
  assertEqual "num props" (Prelude.length props) 11

testParseXClass = TestCase $ withFile "test/classes/XWiki.XWikiServerClass" ReadMode $ \h ->
  parseXClass (sourceHandle h) >>= assertServerClass

testParseXClassJson = TestCase $ withFile "test/classes/XWiki.XWikiServerClass.json" ReadMode $ \h -> do
  json <- BL.hGetContents h
  case eitherDecode json of
    Right xc -> assertServerClass xc
    Left m -> fail m

testXobj2formdata = TestCase $ withFile "test/classes/FAQCode.FAQClass.json" ReadMode $ \h -> do
  json <- BL.hGetContents h
  case eitherDecode "{\"answer\":\"foo\"}" of
    Left m -> fail m
    Right v ->
      case eitherDecode json of
        Right xc -> assertEqual "xobj2formdata" (xobj2formdata xc v) $ Right [("className", "FAQCode.FAQClass"),
                                                                              ("property#answer", "foo")]
        Left m -> fail m

testParseXobj = TestCase $ withFile "test/objects/server-local.json" ReadMode $ \local ->
  withFile "test/objects/Server.json" ReadMode $ \remote -> do
    jsonLocal <- BL.hGetContents local
    jsonRemote <- BL.hGetContents remote
    case (eitherDecode jsonLocal, eitherDecode jsonRemote) of
      (Right localobj, Right remoteobj) -> assertEqual "remote and local parsing produce equal result" (localobj :: XObj) remoteobj
      (Left m, _) -> fail ("local " <> m)
      (_, Left m) -> fail ("remote " <> m)
    

testParseAndSerializeObj = TestCase $ do
  let entity = "wiki:Space.Page^Foo.Class[1]" :: T.Text
  let eobj = parseEntity entity
  assertEqual "parseEntity object" (Right (ObjectEntity (ObjectReference (DocumentReference (SpaceReference (WikiReference "wiki") ["Foo"]) "Class") (DocumentReference (SpaceReference (WikiReference "wiki") ["Space"]) "Page") (Just 1)))) eobj
  let (Right obj) = eobj
  assertEqual "serializeObjectReference" entity $ toStrict $ toLazyText $ serializeEntityReference obj

main =
  withFile "dist/test/test-output.txt" WriteMode $ \h -> do
    Counts { cases=c, tried=t, errors=e, failures=f } <- runTestTT tests
    if e > 0 || f > 0
      then exitFailure
      else exitSuccess
