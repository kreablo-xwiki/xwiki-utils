{-# LANGUAGE LambdaCase #-}

module Data.Sequential(
  (|-->>), (+-->>), (?-->>), (!-->>), initSequential, addSequential, addSequentialOpt
) where

import Control.Monad.Primitive
import Data.Primitive.MutVar

initSequential :: PrimMonad m => (a -> b) -> m (Maybe a) -> m (m (Maybe b), [m (Maybe ())])
initSequential f = addSequential (pure (Just f), [])

addSequentialDef :: PrimMonad m => (m (Maybe (a -> b)), [m (Maybe ())]) -> (Maybe a, m (Maybe a)) -> m (m (Maybe b), [m (Maybe ())])
addSequentialDef (mf, ms) (defaulta, ma) = do
  ref <- newMutVar defaulta
  let mb = do
        f <- mf
        a <- readMutVar ref
        pure $ f <*> a
  let m = ma >>= \case
        a'@(Just _) -> writeMutVar ref a' >> pure (Just ())
        Nothing     -> pure Nothing
  pure (mb, m:ms)

addSequential :: PrimMonad m => (m (Maybe (a -> b)), [m (Maybe ())]) -> m (Maybe a) -> m (m (Maybe b), [m (Maybe ())])
addSequential p ma = addSequentialDef p (Nothing, ma)

addSequentialOpt :: PrimMonad m => (m (Maybe (Maybe a -> b)), [m (Maybe ())]) -> m (Maybe a) -> m (m (Maybe b), [m (Maybe ())])
addSequentialOpt (mf, ms) ma = do
  ref <- newMutVar Nothing
  let mb = do
        f <- mf
        a <- readMutVar ref
        pure $ fmap ($ a) f
  let m = ma >>= \case
        a'@(Just _) -> writeMutVar ref a' >> pure (Just ())
        Nothing     -> pure Nothing
  pure (mb, m:ms)


infix 9 |-->>
(|-->>) :: PrimMonad m => (a -> b) -> m (Maybe a) -> m (m (Maybe b), [m (Maybe ())])
(|-->>) = initSequential
infixl 8 +-->>
(+-->>) :: PrimMonad m => m (m (Maybe (a -> b)), [m (Maybe ())]) -> m (Maybe a) -> m (m (Maybe b), [m (Maybe ())])
m +-->> s =  m >>= flip addSequential s

infixl 8 ?-->>
(?-->>) :: PrimMonad m => m (m (Maybe (Maybe a -> b)), [m (Maybe ())]) -> m (Maybe a) ->  m (m (Maybe b), [m (Maybe ())])
m ?-->> s =  m >>= flip addSequentialOpt s

infixl 8 !-->>
(!-->>) :: PrimMonad m => m (m (Maybe (a -> b)), [m (Maybe ())]) -> (a, m (Maybe a)) -> m (m (Maybe b), [m (Maybe ())])
m !-->> (d, s) =  m >>= flip addSequentialDef (Just d, s)
