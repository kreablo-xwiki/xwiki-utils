{-# LANGUAGE LambdaCase #-}

module XWiki.Util.XML (
  xwikiXmlEscaper,
  escapedElement,
  element,
  element',
  empty,
  time,
  timeMilliSeconds,
  elementWithChildren,
  bool2str
) where

import qualified Text.XML.HaXml.Escape as Escape
import qualified Text.XML.HaXml.Types as X
import Data.Time.Clock
import qualified Data.Time.Format as Time

xwikiXmlEscaper :: Escape.XmlEscaper
xwikiXmlEscaper = Escape.mkXmlEscaper
   [('\60',"lt"),('\62',"gt"),('\38',"amp"),('\39',"apos"),('\34',"quot")]
   (\case
       '&' -> True
       '<' -> True
       '>' -> True
       _ -> False
   )

escapedElement :: String -> [X.Attribute] -> [X.Content i] -> X.Element i
escapedElement tag attr content = Escape.xmlEscape xwikiXmlEscaper $ X.Elem (X.N tag) attr content

element :: String -> String -> X.Content ()
element tag value = X.CElem (escapedElement tag [] [X.CString False value ()]) ()

empty :: String -> X.Content ()
empty tag = X.CElem (X.Elem (X.N tag) [] []) ()

element' :: Show a => String -> a -> X.Content ()
element' tag value = X.CElem (escapedElement tag [] [X.CString False (show value) ()]) ()

time :: String -> UTCTime -> X.Content()
time tag value = X.CElem (escapedElement tag [] [X.CString False (show $ timeMilliSeconds value) ()]) ()

timeMilliSeconds :: UTCTime -> Integer
timeMilliSeconds time' =
  let Just t' = Time.parseTimeM True Time.defaultTimeLocale "%s" "0" in
    round (1000 * diffUTCTime time' t')

elementWithChildren :: String -> [X.Content ()] -> X.Content ()
elementWithChildren tag children' = X.CElem (escapedElement tag [] children') ()

bool2str :: Bool -> String
bool2str True  = "true"
bool2str False = "false"


