module XWiki.Util.HasXML(
  HasXML(..)
  ) where

import qualified Text.XML.HaXml.Types as X

class HasXML a where
  toXML :: a -> X.Element ()

