{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
module XWiki.XClass(
  parseXClass,
  XClass(..),
  XProperty(..),
  XPropertyValues(..)
) where

import Data.XML.Types
import Text.XML
import Text.XML.Stream.Parse as XMLParse
import Control.Monad.Catch
import Data.Conduit
import Data.Text
import qualified Data.ByteString as B
import Control.Monad.Primitive
import Control.Monad
import Text.Read
import Control.Monad.Trans
import Data.Maybe
import Data.Either
import Data.Aeson as A
import Data.Aeson.Types as AT
import Data.Sequential
import qualified Data.HashMap.Strict as Map
import qualified Data.Map as LMap
import Control.Applicative
import XWiki.XPropertyType
import qualified Data.Text as T
import Text.Parsec as Parsec hiding ((<|>))

data XClass = XClass {
  xclassId     :: Text,
  xclassName   :: Text,
  xclassProperties :: [XProperty]
} deriving Show


data XProperty = XProperty {
  xpropName         :: Text,
  xpropType         :: XPropertyType,
  xpropPrettyName   :: Text,
  xpropUnmodifiable :: Bool,
  xpropDisabled     :: Bool,
  xpropSize         :: Maybe Int,
  xpropNumber       :: Int,
  xpropValues       :: [(Text, Maybe Text)]
} deriving Show

newtype XPropertyValues = XPropertyValues (LMap.Map Text (Maybe Text)) deriving Show

instance FromJSON XPropertyValues where
  parseJSON = withObject "XPropertyValues" $ \o -> XPropertyValues <$>
    (o .: "propertyValues" >>= pvs)

    where
      pvs = fmap LMap.fromList . mapM pv
      pv = withObject "propertyValues" $ \o' -> (,) <$> o' .: "value" <*> (o' .: "metaData" >>= label')
      label' = withObject "label" $ \o' -> o' .:? "label"

instance FromJSON XProperty where
    parseJSON = withObject "XProperty" $ \o -> XProperty <$>
      o .: "name" <*>
      (propType <$> o .: "type" >>= \case Nothing -> mzero
                                          Just t -> pure t) <**>
      (o .: "attributes" >>= withAttributeMap attrs)

      where
        attrs l =
          let mpn = l "prettyName"
              mu  = l "unmodifiable"
              md  = l "disabled" 
              ms  = l "size"
              mn  = l "number"
              vs = values' $ l "values"
          in
            case (mpn, toBool =<< mu, toBool =<< md, toInt =<< mn) of
              (Just pn, Just u, Just d, Just n) -> pure $  ($ vs) . ($ n) . ($ toInt =<< ms) . ($ d) . ($ u) . ($ pn)
              _                                 -> mzero

        values' :: Maybe Text -> [(Text, Maybe Text)]
        values' = maybe [] values

values :: Text -> [(Text, Maybe Text)]
values = fromRight [] . runParser vs () "Values"
  where
    vs' = (\key value -> (key, value)) <$> k <*> v
    vs = sepBy vs' $ char '|'
    k = T.pack <$> many1 (noneOf "=|")
    v = optionMaybe (char '=' >> T.pack <$> Parsec.many (noneOf "|"))

instance FromJSON XClass where
  parseJSON = withObject "XClass" $ \o -> XClass <$>
    o .: "id" <*>
    o .: "name" <*>
    o .: "properties"


attributeMap :: Array -> Maybe (Text -> Maybe Text)
attributeMap a = flip Map.lookup <$> Prelude.foldr insert' (pure Map.empty) a
  where
    insert' obj m = nameval obj <*> pure Map.insert <*> m <|> m

    nameval (Object o) = do
      name' <- Map.lookup "name" o
      value' <- Map.lookup "value" o
      case (name', value') of
        (AT.String name, AT.String value) -> pure $ ($ value) . ($ name)
        _                                 -> Nothing
    nameval _ = Nothing

withAttributeMap :: ((Text -> Maybe Text) -> Parser a) -> Value -> Parser a
withAttributeMap p = withArray "Attributes" (flip force' p . attributeMap)
  where
    force' (Just v) = ($ v)
    force' _        = const mzero

--type XMLPosParser m a = ConduitT EventPos Event m a
type XMLParser m a = ConduitT Event Void m a

x :: Text -> NameMatcher Name
x s  = NameMatcher $ \name@(Name localName nameSpace prefix) -> if localName == s &&
                                                                   nameSpace == Just "http://www.xwiki.org" &&
                                                                   isNothing prefix
                                                                then Just name
                                                                else Nothing
                                               
parseXClass :: (MonadThrow m, PrimMonad m, MonadIO m) => ConduitT () B.ByteString m () -> m XClass
parseXClass source = runConduit $ source .| parseBytes def { psRetainNamespaces = False } .| force "Failed to parse XWiki class xml" xclass

xclass :: (MonadThrow m, PrimMonad m) => XMLParser m (Maybe XClass)
xclass = tagIgnoreAttrs (x "class") body
  where
    body = do
      many_ (ignoreTreeContent (x "link"))
      XClass <$> force "Expecting id element" (tagNoAttr (x "id") content)
        <*> force "Expecting name element" (tagNoAttr (x "name") content)
        <*> XMLParse.many xproperty

err :: MonadThrow m => String -> m a
err msg = force msg $ pure Nothing

xproperty :: (MonadThrow m, PrimMonad m) => XMLParser m (Maybe XProperty)
xproperty = tag' (x "property") (XProperty <$> requireAttr "name" <*> proptype) body
  where
    proptype = requireAttr "type" >>= \attr' -> case propType attr' of
      Nothing -> err $ "Invalid attribute: " <> show attr'
      Just t -> pure t
    body p = do
      many_ $ ignoreTreeContent (x "link")
      (r, bps) <- p |-->> pa "prettyName" Just +-->> pa "disabled" toBool +-->> pa "unmodifiable" toBool ?-->> pa "size" toInt +-->> pa "number" toInt !-->> ([], pa "values" (Just . values))
      _ <- XMLParse.many $ choose (bps <> [ignoreEmptyTag (x "attribute")])
      force "Expecting property attribute elements" r
    
    pa n f = tag' (x "attribute") (do
                        name <- requireAttr "name"
                        when (
                          name /= n) $ err ("Expecting <attribute name=\"" <> unpack n <> "\"/>")
                        force ("Invalid value attribute for <attribute name=\"" <> unpack n <> "\"/>.") (f <$> requireAttr "value")) pure

toInt :: Text -> Maybe Int
toInt = readMaybe . unpack

toBool :: Text -> Maybe Bool
toBool = fmap (\case 0   -> False
                     _   -> True) . toInt

