{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleInstances #-}

module XWiki.Data.XWikiObject (
  XWikiObject,
  XWikiObject'(..),
  XWikiObjProperties,
  XWikiObjProperties',
  XWikiObjProperty'(..),
  XWikiObjProperty,
  XWikiClass
) where

import Data.UUID
import XWiki.Util.HasXML
import XWiki.Util.XML
import qualified Data.Map as Map
import qualified Text.XML.HaXml.Types as X


type XWikiObjProperties' a = Map.Map String (XWikiObjProperty' a)
type XWikiObjProperties = XWikiObjProperties' ()

data XWikiObjProperty' a = PlainString String | ValueList [String] | PostProcessedString (a -> String)
type XWikiObjProperty = XWikiObjProperty' ()

type XWikiClass = X.Content ()

data XWikiObject' a = XObj {
  xobjClass :: XWikiClass,
  xobjName :: String,
  xobjNumber :: Int,
  xobjClassName :: String,
  xobjGuid :: UUID,
  xobjProperties ::  XWikiObjProperties' a
}

type XWikiObject = XWikiObject' ()

instance HasXML (XWikiObject' ()) where
  toXML o = X.Elem (X.N "object") [] c where
    c = xobjClass o:
        element "name" (xobjName o):
        element' "number" (xobjNumber o):
        element "className" (xobjClassName o):
        element' "guid" (xobjGuid o) :
                   fmap (\(tagName, content) -> elementWithChildren "property"
                                                [elementWithChildren tagName (xobjPropertyValueXML content)])
                                            (Map.assocs $ xobjProperties o)

xobjPropertyValueXML :: XWikiObjProperty -> [X.Content ()]
xobjPropertyValueXML (PlainString s) = [X.CString False s ()]
xobjPropertyValueXML (ValueList []) = []
xobjPropertyValueXML (ValueList (v:vs)) = element "value" v:xobjPropertyValueXML (ValueList vs)
xobjPropertyValueXML (PostProcessedString pp) = [X.CString False (pp ()) ()]

