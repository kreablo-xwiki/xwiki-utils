{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module XWiki.Data.XWikiDocument (
  XWikiDocument,
  XWikiDocument'(..),
  escapeDocName,
  cleanDocName,
  xwikiDocumentXml
) where


import Data.Time.Clock
import XWiki.Data.XWikiObject
import Data.Char
import XWiki.Util.HasXML
import XWiki.Util.XML
import qualified Text.XML.HaXml.Types as X

data XWikiDocument' a = XDoc {
  xdocSpace :: String,
  xdocName :: String,
  xdocLanguage :: String,
  xdocDefaultLanguage :: String,
  xdocTranslation :: Int,
  xdocParent :: String,
  xdocCreator :: String,
  xdocAuthor :: String,
  xdocCustomClass :: String,
  xdocContentAuthor :: String,
  xdocCreationDate :: UTCTime,
  xdocDate :: UTCTime,
  xdocContentUpdateDate :: UTCTime,
  xdocVersion :: (Int, Int),
  xdocTitle :: String,
  xdocDefaultTemplate :: String,
  xdocValidationScript :: String,
  xdocComment :: String,
  xdocMinorEdit :: Bool,
  xdocSyntaxId :: String,
  xdocHidden :: Bool,
  xdocObject :: [XWikiObject' a],
  xdocContent :: Either String (a -> String)
}

type XWikiDocument = XWikiDocument' ()

---
--- Escape function for document name fragments
---
escapeDocName :: String -> String
escapeDocName [] = []
escapeDocName ('.':rest) = '\\':'.':escapeDocName rest
escapeDocName ('\\':rest) = '\\':'\\':escapeDocName rest
escapeDocName (c:rest) = c:escapeDocName rest


cleanDocName :: String -> String
cleanDocName = take 220 . filter (\c -> isPrint c && c /= '/' && c /= '&' && c /= '?' && c /= '\194' && c /= ':' && c /= ',' && c /= '.') . map    (\c -> if c == '\160' then ' ' else c)

instance HasXML XWikiDocument where
  toXML d = X.Elem (X.N "xwikidoc") [] c where
    c =
      element "web" (xdocSpace d):
      element "name" (xdocName d):
      element "language" (xdocLanguage d):
      element "defaultLanguage" (xdocDefaultLanguage d):
      element' "translation" (xdocTranslation d):
      element "parent" (xdocParent d):
      element "creator" (xdocCreator d):
      element "author" (xdocAuthor d):
      element "customClass" (xdocCustomClass d):
      element "contentAuthor" (xdocContentAuthor d):
      time "creationDate" (xdocCreationDate d):
      time "date" (xdocDate d):
      time "contentUpdateDate" (xdocDate d):
      element "version" (let (major, minor) = xdocVersion d in show major ++ "." ++ show minor):
      element "title" (xdocTitle d):
      element "defaultTemplate" (xdocDefaultTemplate d):
      element "validationScript" (xdocValidationScript d):
      element "comment" (xdocComment d):
      element  "minorEdit" (bool2str $ xdocMinorEdit d):
      element "syntaxId" (xdocSyntaxId d):
      element "hidden" (bool2str $ xdocHidden d):
      map ((`X.CElem` ()) . toXML) (xdocObject d) ++
      [element "content" $ (\case
                               Left c' -> c'
                               Right _ -> "<<ERROR: cannot realize content here!>>" )  (xdocContent d)]



xwikiDocumentXml :: X.Element i -> X.Document i
xwikiDocumentXml root = X.Document (X.Prolog (Just (X.XMLDecl "1.0" (Just (X.EncodingDecl "UTF-8")) Nothing))
                                    []
                                    Nothing
                                    []) X.emptyST root []
