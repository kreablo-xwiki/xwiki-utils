module XWiki.Data.XAR (
  XARPackage(..),
  newXARPackage,
  XARFile(..),
  newXARFile,
  addXWikiDocument,
  writeXWikiDocument,
  writeXAR
) where

import XWiki.Util.XML
import XWiki.Data.XWikiDocument
import Data.Time.Clock
import qualified Text.XML.HaXml.Types as X
import qualified Text.XML.HaXml.Pretty as PrettyXML
import qualified Data.ByteString.Lazy.UTF8 as BSUTF8
import qualified Data.ByteString.Lazy.Char8 as B
import qualified Data.ByteString.UTF8 as BSUTF8Strict
--import qualified System.Directory as Directory
--import qualified System.Posix.Files as Files
--import qualified System.FilePath as FP
import qualified Codec.Binary.QuotedPrintable as QP
import Codec.Archive.Zip
import XWiki.Util.HasXML

data XARPackage = XARPackage {
  xarInfos :: (),
  xarName :: String,
  xarDescription :: String,
  xarLicence :: String,
  xarAuthor :: String,
  xarVersion :: String,
  xarBackupPack :: Bool,
  xarPreserveVersion :: Bool,
  xarFiles :: [XARFile],
  xarDocuments :: [XWikiDocument]
}

newXARPackage :: String -> XARPackage
newXARPackage name = XARPackage {
  xarInfos = (),
  xarName = name,
  xarDescription = "",
  xarLicence = "",
  xarAuthor = "TVL Converter Application",
  xarVersion = "1.0.0",
  xarBackupPack = False,
  xarPreserveVersion = False,
  xarFiles = [],
  xarDocuments = []}

data XARFile = XARFile {
  xarfileDefaultAction :: Int,
  xarfileLanguage :: String,
  xarfileFilename :: String
}

newXARFile :: String -> XARFile
newXARFile filename = XARFile {xarfileDefaultAction = 0, xarfileLanguage="", xarfileFilename=filename}

instance HasXML XARPackage where
  toXML package =  X.Elem (X.N "package") [] (c package) where
    c d = [
     empty "infos",
     element "name" (xarName d),
     element "description" (xarDescription d),
     element "licence" (xarLicence d),
     element "author" (xarAuthor d),
     element "version" (xarVersion d),
     element "backupPack" (bool2str $ xarBackupPack d),
     element "preserveVersion" (bool2str $ xarPreserveVersion d),
     elementWithChildren "files" (map (\f -> X.CElem (toXML f) ()) $ xarFiles d) ]


instance HasXML XARFile where
  toXML xarFile = X.Elem (X.N "file")
                   [(X.N "defaultAction", X.AttValue [Left (show $ xarfileDefaultAction xarFile)]),
                    (X.N "language", X.AttValue [Left (xarfileLanguage xarFile)])]
                   [X.CString False (xarfileFilename xarFile) ()]


addXWikiDocument :: XARPackage -> XWikiDocument -> XARPackage
addXWikiDocument package doc =
  let name = escapeDocName (xdocSpace doc) ++ "." ++ escapeDocName (xdocName doc) in
    package {
      xarFiles     = newXARFile name:xarFiles package,
      xarDocuments = doc:xarDocuments package  }


writeXWikiDocument :: UTCTime -> XWikiDocument -> Archive -> Archive
writeXWikiDocument time' doc archive =
  let content = BSUTF8.fromString $ show (PrettyXML.document $ xwikiDocumentXml $ toXML doc)
      name = encodeFilename (xdocSpace doc) ++ "/" ++ encodeFilename (xdocName doc) ++ ".xml"
      entry = toEntry name (round(toRational(timeMilliSeconds time') / 1000)) content in
  addEntryToArchive entry archive

writeXWikiDocuments :: UTCTime -> [XWikiDocument] -> Archive -> Archive
writeXWikiDocuments _ [] archive = archive
writeXWikiDocuments time' (doc:docs) archive =
  writeXWikiDocuments time' docs $ writeXWikiDocument time' doc archive

encodeFilename :: String -> String
encodeFilename = take 200 . BSUTF8Strict.toString . QP.encode . BSUTF8Strict.fromString

writeXAR :: String -> XARPackage -> IO ()
writeXAR filename package =
   do
    time' <- getCurrentTime
    let
      packageContent = BSUTF8.fromString $ show (PrettyXML.document $ xwikiDocumentXml $ toXML package)
      packageEntry = toEntry "package.xml" (round(toRational(timeMilliSeconds time') / 1000)) packageContent
      archive = writeXWikiDocuments time' (xarDocuments package)
                  (addEntryToArchive packageEntry emptyArchive)
    B.writeFile filename (fromArchive archive)
    return ()
