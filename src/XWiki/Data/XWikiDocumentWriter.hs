module XWiki.Data.XWikiDocumentWriter (
  newXWikiDocument,
  XWikiDocumentWriterState(..),
  XWikiDocumentWriter
) where

import XWiki.Data.XWikiDocument
import Control.Monad.Writer
import Control.Monad.State
import Data.Time

type XWikiDocumentWriter s a = StateT s (Writer [XWikiDocument' s]) a

class XWikiDocumentWriterState s where
  xwdwsTime :: s -> UTCTime
  xwdwsDocName :: s -> String
  xwdwsDocTitle :: s -> String
  xwdwsDocSpace :: s -> String

newXWikiDocument :: XWikiDocumentWriterState s => XWikiDocumentWriter s (XWikiDocument' a)
newXWikiDocument = do
  time' <- gets xwdwsTime
  docName <- gets xwdwsDocName
  title <- gets xwdwsDocTitle
  space <- gets xwdwsDocSpace
  return XDoc {
    xdocSpace = space,
    xdocName = cleanDocName docName,
    xdocLanguage = "",
    xdocDefaultLanguage = "sv",
    xdocTranslation = 0,
    xdocParent = "",
    xdocCreator = "XWiki.Admin",
    xdocAuthor = "XWiki.Admin",
    xdocContentAuthor = "XWiki.Admin",
    xdocCustomClass = "",
    xdocCreationDate = time',
    xdocDate = time',
    xdocContentUpdateDate = time',
    xdocTitle = title,
    xdocDefaultTemplate = "",
    xdocVersion = (1, 1),
    xdocValidationScript = "",
    xdocComment = "",
    xdocMinorEdit = False,
    xdocSyntaxId = "xwiki/2.1",
    xdocHidden = False,
    xdocObject = [],
    xdocContent = Left ""
}
