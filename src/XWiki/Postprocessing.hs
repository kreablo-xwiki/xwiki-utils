module XWiki.Postprocessing(
  postprocessDocument
) where

import XWiki.Data.XWikiDocument
import XWiki.Data.XWikiObject
import qualified Data.Map as Map

postprocessDocument :: a -> XWikiDocument' a -> XWikiDocument
postprocessDocument s d =
  let objs = postprocessObjects s $ xdocObject d
      content = case xdocContent d of
        Right postprocessor -> Left $ postprocessor s
        Left content'       -> Left content'
      in
    XDoc {
      xdocSpace = xdocSpace d,
      xdocName = xdocName d,
      xdocLanguage = xdocLanguage d,
      xdocDefaultLanguage = xdocDefaultLanguage d,
      xdocTranslation = xdocTranslation d,
      xdocParent = xdocParent d,
      xdocCreator = xdocCreator d,
      xdocAuthor = xdocAuthor d,
      xdocCustomClass = xdocCustomClass d,
      xdocContentAuthor = xdocContentAuthor d,
      xdocCreationDate = xdocCreationDate d,
      xdocDate = xdocDate d,
      xdocContentUpdateDate = xdocContentUpdateDate d,
      xdocVersion = xdocVersion d,
      xdocTitle = xdocTitle d,
      xdocDefaultTemplate = xdocDefaultTemplate d,
      xdocValidationScript = xdocValidationScript d,
      xdocComment = xdocComment d,
      xdocMinorEdit = xdocMinorEdit d,
      xdocSyntaxId = xdocSyntaxId d,
      xdocHidden = xdocHidden d,
      xdocObject = objs,
      xdocContent = content
      }    

postprocessObjects :: a -> [XWikiObject' a] -> [XWikiObject]
postprocessObjects s = fmap pp
  where
    pp o@XObj { xobjProperties = props } = XObj {
        xobjClass = xobjClass o,
        xobjName = xobjName o,
        xobjNumber = xobjNumber o,
        xobjClassName = xobjClassName o,
        xobjGuid = xobjGuid o,
        xobjProperties = postprocessProperties s props
      }

postprocessProperties :: a -> XWikiObjProperties' a -> XWikiObjProperties
postprocessProperties s =
  Map.foldrWithKey (\k p m -> Map.insert k (case p of
                                               PostProcessedString postprocessor -> PlainString $ postprocessor s
                                               PlainString s'                    -> PlainString s'
                                               ValueList   v                     -> ValueList v) m) Map.empty


