{-# LANGUAGE OverloadedStrings #-}

module XWiki.XClassValidation(
  validateXClass,
  xobj2formdata,
  serializeFormdata,
  xpropdata
) where

import qualified Data.ByteString.Lazy as LB
import qualified Data.ByteString.Builder as BB
import Web.Internal.HttpApiData
import Text.Printf
import XWiki.XClass
import XWiki.XObj
import XWiki.XPropertyType
import XWiki.Rest.Class
import Data.Text
import Data.Aeson.Types as AT
import qualified Data.HashMap.Strict as Map
import qualified Data.Map as LMap
import qualified Data.Text as T
import Data.Maybe
import Data.Foldable

validateXClass :: XWikiRest r => XClass -> XObj -> r [Text]
validateXClass xc (XObj (Object o)) = foldrM (\a b -> (<> b) <$> validate a) [] $ xclassProperties xc
  where
    validate cprop =
      let
        name   = xpropName cprop
        moprop = Map.lookup name o
        disabled = xpropDisabled cprop
      in
        case (moprop, disabled) of
          (_, True)       -> pure []
          (Nothing, _)    -> pure ["Missing property: '" <> name <> "'"]
          (Just oprop, _) -> validateProp oprop cprop

    validateProp oprop XProperty {
      xpropName = name,
      xpropType = XPropertyTypeStaticList,
      xpropValues = vs
      } = case xpropdata XPropertyTypeStaticList oprop of
            Left e -> pure e
            Right v -> pure $ if isJust $ lookup v vs then [] else ["Value '" <> v <> "' is not a legal value for static list property '" <> name <> "'.  (" <>
                                                                    T.intercalate "|" (fmap fst vs) <> ")"]

    validateProp oprop XProperty {
      xpropName = name,
      xpropType = XPropertyTypeDatabaseList
      } = case xpropdata XPropertyTypeDatabaseList oprop of
            Left e -> pure e
            Right v -> do
              evs <- getPropertyValues xc name
              case evs of
                Right (XPropertyValues vs) -> case LMap.lookup v vs of
                  Just _  -> pure []
                  Nothing -> pure ["Value '" <> v <> "' is not a legal value for database list property '" <> name <> "'. (" <>
                                  T.intercalate "|" (LMap.keys vs) <> ")"]
                Left e -> pure [pack e]

    validateProp _oprop _cprop = pure []

validateXClass _ _ = pure ["Object expected!"]

xobj2formdata :: XWikiRest r => XClass -> XObj -> r (Either [Text] [(Text, Text)])
xobj2formdata xc xo@(XObj (Object o)) = do
  valid <- validateXClass xc xo
  pure $ case valid of
    errs@(_:_) -> Left errs
    []         -> Prelude.foldr propdata (Right [("className", xclassName xc)]) $ xclassProperties xc
  where
    propdata :: XProperty -> Either [Text] [(Text, Text)] -> Either [Text] [(Text, Text)]
    propdata cprop a =
      let
        pn = xpropName cprop
      in
        case (Map.lookup pn o, xpropDisabled cprop) of
          (_,       True)     -> a
          (Nothing, False)    -> Left ["Missing property: '" <> pn <> "'"]
          (Just oprop, False) -> (\a' pd -> a' <> [("property#" <> pn, pd)]) <$> a <*> propdata' cprop oprop
    propdata' :: XProperty -> Value -> Either [Text] Text
    propdata' = xpropdata . xpropType
xobj2formdata xc v = Left <$> validateXClass xc v

serializeFormdata :: [(Text, Text)] -> LB.ByteString
serializeFormdata = BB.toLazyByteString . intercalate' "&" . fmap nv
  where
    nv (name, value) =  toEncodedUrlPiece name <> "=" <> toEncodedUrlPiece value
    intercalate' _ [] = ""
    intercalate' _ [b] = b
    intercalate' c (b:bs) = b <> c <> intercalate' c bs

xpropdata :: XPropertyType -> Value -> Either [Text] Text
xpropdata XPropertyTypeTimezone _ = Left ["Timezone type property support not implemented."]
xpropdata XPropertyTypeEmail (AT.String t) = pure t
xpropdata XPropertyTypeEmail _ = Left ["Email type property requires a string value."]
xpropdata XPropertyTypeTextArea (AT.String t) = pure t
xpropdata XPropertyTypeTextArea _ = Left ["Text area type property requires a string value."]
xpropdata XPropertyTypeDatabaseTree _ = Left ["Database tree type property support not implemented."]
xpropdata XPropertyTypePage (AT.String s) =  pure s
xpropdata XPropertyTypePage _ =  Left ["Page type property requires a string value."]
xpropdata XPropertyTypeComputedField _ =  Left ["Computed field type property support not implemented."]
xpropdata XPropertyTypeString (AT.String s) = pure s
xpropdata XPropertyTypeString _ = Left ["String type property requires a string value."]
xpropdata XPropertyTypeUsers _ =  Left ["Users type property support not implemented."]
xpropdata XPropertyTypeDate (AT.String s) = pure s
xpropdata XPropertyTypeDate _ =  Left ["Date type property requires a string value."]
xpropdata XPropertyTypeDatabaseList (AT.String s) = pure s
xpropdata XPropertyTypeDatabaseList _ =  Left ["Database list type property requires a string value."]
xpropdata XPropertyTypeNumber (AT.Number s) = pure $ pack $ printf "%d" (floor s :: Integer)
xpropdata XPropertyTypeNumber _ = Left ["Number type property requires a number value."]
xpropdata XPropertyTypeStaticList (AT.String s) = pure s
xpropdata XPropertyTypeStaticList _ = Left ["Static list type property requires a string value."]
xpropdata XPropertyTypeAccessRightLevels _ = Left ["Access right levels type property support not implemented."]
xpropdata XPropertyTypeBoolean (AT.Bool b)  = pure $ if b then "1" else "0"
xpropdata XPropertyTypeBoolean _ = Left ["Boolean type property requires a boolean value."]
xpropdata XPropertyTypePassword (AT.String t) = pure t
xpropdata XPropertyTypePassword _ = Left ["Password type property requires a string value."]

