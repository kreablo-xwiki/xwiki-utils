{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
module XWiki.XPropertyType (
  XPropertyType(..),
  propType
) where

import Data.Aeson
import Data.Text

data XPropertyType = XPropertyTypeTimezone          |
                     XPropertyTypeEmail             |
                     XPropertyTypeTextArea          |
                     XPropertyTypeDatabaseTree      |
                     XPropertyTypePage              |
                     XPropertyTypeComputedField     |
                     XPropertyTypeString            |
                     XPropertyTypeUsers             |
                     XPropertyTypeDate              |
                     XPropertyTypeDatabaseList      |
                     XPropertyTypeNumber            |
                     XPropertyTypeStaticList        |
                     XPropertyTypeAccessRightLevels |
                     XPropertyTypeBoolean           |
                     XPropertyTypePassword deriving Show



propType :: Text -> Maybe XPropertyType
propType = \case
  "Timezone"      -> Just XPropertyTypeTimezone
  "Email"         -> Just XPropertyTypeEmail
  "TextArea"      -> Just XPropertyTypeTextArea
  "DBTreeList"    -> Just XPropertyTypeDatabaseTree
  "Page"          -> Just XPropertyTypePage
  "ComputedField" -> Just XPropertyTypeComputedField
  "String"        -> Just XPropertyTypeString
  "Users"         -> Just XPropertyTypeUsers
  "Date"          -> Just XPropertyTypeDate
  "DBList"        -> Just XPropertyTypeDatabaseList
  "Number"        -> Just XPropertyTypeNumber
  "StaticList"    -> Just XPropertyTypeStaticList
  "Levels"        -> Just XPropertyTypeAccessRightLevels
  "Boolean"       -> Just XPropertyTypeBoolean
  "Password"      -> Just XPropertyTypePassword
  _               -> Nothing

instance FromJSON XPropertyType where

  parseJSON = withText "property type" $ \t -> case propType t of
    Nothing -> fail $ "Not a property type: " <> show t
    Just pt -> pure pt

