{-# LANGUAGE OverloadedStrings #-}
module XWiki.XObj(
  XObj(..)
) where

import Data.Aeson as A
import Data.Aeson.Types as AT
import qualified Data.HashMap.Strict as Map
import Control.Monad
import XWiki.XPropertyType
import Data.Text
import Text.Read

newtype XObj = XObj AT.Value deriving (Eq, Show)


instance FromJSON XObj where
  parseJSON = withObject "XObj" $ \o ->
    case Map.lookup "properties" o of
      Just (Array p) -> remoteObj p
      _              -> localObj o

      where
        localObj o = pure $ XObj $ AT.Object o
        remoteObj p = XObj . AT.Object <$> foldM prop Map.empty p
        prop obj = withObject "property" $ \o -> do
          let name' = Map.lookup "name" o
          let value' = Map.lookup "value" o
          let type' = Map.lookup "type" o
          case (name', value', type') of
            (Just (AT.String name), Just value, Just t) -> do
              t' <- convert t value
              pure $ Map.insert name t' obj
            (Just v, Just _, Just _)               -> typeMismatch "String" v
            (Nothing, _, _)                        -> fail "Property is missing name attribute"
            (_, Nothing, _)                        -> fail "Property is missing value attribute"
            (_, _, Nothing)                        -> fail "Property is missing type attribute"
        convert pt v = case fromJSON pt of
          Success t -> convert' t v
          Error e   -> fail e

        convert' t v = case t of
                          XPropertyTypeTimezone -> pure v
                          XPropertyTypeEmail    -> pure v
                          XPropertyTypeTextArea -> pure v
                          XPropertyTypeDatabaseTree -> pure v
                          XPropertyTypePage -> pure v
                          XPropertyTypeComputedField -> pure v
                          XPropertyTypeString -> pure v
                          XPropertyTypeUsers -> pure v
                          XPropertyTypeDate -> pure v
                          XPropertyTypeDatabaseList -> pure v
                          XPropertyTypeNumber -> case v of
                            (AT.Number _) -> pure v
                            (AT.String "") -> pure AT.Null
                            (AT.String s) -> case readMaybe $ unpack s of
                              Just n -> pure $ AT.Number n
                              _      -> fail $ unpack $ "Could not read number " <> s
                            _             -> typeMismatch "String" v         
                          XPropertyTypeStaticList -> pure v
                          XPropertyTypeAccessRightLevels -> pure v
                          XPropertyTypeBoolean -> case v of
                            (AT.Bool _) -> pure v
                            (AT.String "") -> pure AT.Null
                            (AT.String s) -> pure $ AT.Bool $ s == "1"
                            _             -> typeMismatch "String" v         
                          XPropertyTypePassword -> pure v

