{-# LANGUAGE OverloadedStrings #-}
module XWiki.Rest.Space (
  XWikiRestLink(..),
  XWikiRestSpace(..),
  XWikiRestSpaces(..)
) where

import Data.Text(Text)
import Data.Aeson
import Data.Aeson.Types(typeMismatch)

data XWikiRestLink = XWikiRestLink {
  xwrlHref :: Text,
  xwrlRel :: Text,
  xwrlHrefLang :: Maybe Text,
  xwrlType :: Maybe Text
}

data XWikiRestSpace = XWikiRestSpace {
  xwsName :: Text,
  xwsId :: Text,
  xwsWiki :: Text,
  xwsLinks :: [XWikiRestLink],
  xwsHome :: Maybe Text,
  xwsXWikiAbsoluteUrl :: Maybe Text,
  xwsXWikiRelativeUrl :: Maybe Text
}

data XWikiRestSpaces = XWikiRestSpaces {
  xwssLinks :: [XWikiRestLink],
  xwssSpaces :: [XWikiRestSpace]
}

instance FromJSON XWikiRestLink where
  parseJSON (Object v) =
    XWikiRestLink <$> v .: "href"
                  <*> v .: "rel"
                  <*> v .: "hrefLang"
                  <*> v .: "type"

  parseJSON invalid = typeMismatch "XWikiRestLink" invalid

instance ToJSON XWikiRestLink where
  toJSON (XWikiRestLink href rel lang typ) =
    object ["href" .= href,
            "rel" .= rel,
            "hrefLang" .= lang,
            "type" .= typ
           ]


instance FromJSON XWikiRestSpace where
  parseJSON (Object v) =
    XWikiRestSpace
               <$> v .: "name"
               <*> v .: "id"
               <*> v .: "wiki"
               <*> v .: "links"
               <*> v .: "home"
               <*> v .: "xwikiAbsoluteUrl"
               <*> v .: "xwikiRelativeUrl"

  parseJSON invalid = typeMismatch "XWikiRestSpace" invalid

instance ToJSON XWikiRestSpace where
  toJSON (XWikiRestSpace name id' wiki links home absolute relative) =
    object ["name" .= name,
            "id"   .= id',
            "wiki" .= wiki,
            "links" .= links,
            "home" .= home,
            "xwikiAbsoluteUrl" .= absolute,
            "xwikiRelativeUrl" .= relative]

instance FromJSON XWikiRestSpaces where
  parseJSON (Object v) =
    XWikiRestSpaces <$> v .: "links"
                    <*> v .: "spaces"

  parseJSON invalid = typeMismatch "XWikiRestSpaces" invalid

instance ToJSON XWikiRestSpaces where
  toJSON (XWikiRestSpaces links spaces) =
    object ["links" .= links,
            "spaces" .= spaces]

