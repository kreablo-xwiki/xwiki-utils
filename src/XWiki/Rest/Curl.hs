{-# LANGUAGE TupleSections #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module XWiki.Rest.Curl(
  CurlXWikiRest(),
  withXWikiRestDo,
  getUrl,
  getData,
  fileHandleReader
) where

import XWiki.Wiki
import XWiki.Model
import XWiki.Rest.Class
import XWiki.Rest.Space
import XWiki.Rest.Format
import XWiki.XObj
import Data.Functor
import Data.Aeson(eitherDecode, Value(Object, String), encode, withObject, withScientific, withText, (.:))
import Data.Aeson.Types(Parser, parseEither)
import Data.Monoid
import Data.Maybe
import Data.Either
import Data.Traversable
import Data.List((\\))
import Data.IORef
import qualified Data.HashMap.Lazy as HashMap
import qualified Data.Map as Map
import Data.Text(unpack, Text)
import Data.Text.Lazy(toStrict)
import Data.Text.Lazy.Encoding(decodeUtf8, encodeUtf8)
import qualified Data.Text.Lazy as LT
import Data.Text.Lazy.Builder(fromText, toLazyText, toLazyText)
import Control.Applicative
import Control.Monad
import Control.Monad.State
import System.IO (IO, print, Handle, BufferMode(BlockBuffering), IOMode(WriteMode), hSetBuffering, openFile, hClose, putStrLn, hFlush)
import Prelude(Show, div, floor, (&&), (.), ($), (/=), (==), (||), fst, String, show, Int, min, (*), (+), (-), Integral(..), (>), Num(..), undefined, Bool(..))
import Network.Curl
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import Data.ByteString.Builder(toLazyByteString)
import qualified Foreign.Storable as Storable
import Network.HTTP.Types.URI(QueryText, renderQueryText)
import XWiki.XClassValidation
import XWiki.XClass


data CurlXWikiRestState = CurlXWikiRestState {
  cxrsWikiItem :: WikiItem,
  cxrsCurl    :: Curl,
  cxrsCreate  :: Bool,
  cxrsFormat  :: Format,
  cxrsPropertyValues :: Map.Map (Text, Text) XPropertyValues
}
newtype CurlXWikiRest a = CurlXWikiRest { runCurlXWikiRest :: StateT CurlXWikiRestState IO a }

instance Functor CurlXWikiRest where
  fmap f (CurlXWikiRest s) = CurlXWikiRest $ fmap f s

instance Applicative CurlXWikiRest where
  pure = CurlXWikiRest . pure
  CurlXWikiRest sf <*> CurlXWikiRest sx = CurlXWikiRest (sf <*> sx)

instance Monad CurlXWikiRest where

  CurlXWikiRest m >>= sf = CurlXWikiRest $ do
    v <- m
    let CurlXWikiRest mr = sf v
    mr

instance MonadState CurlXWikiRestState CurlXWikiRest where
  state = CurlXWikiRest . state
  get   = CurlXWikiRest get
  put   = CurlXWikiRest . put


instance MonadIO CurlXWikiRest where
  liftIO = CurlXWikiRest . liftIO

withXWikiRestDo :: CurlXWikiRest a -> WikiItem -> IO a
withXWikiRestDo m wi = withCurlDo $ do
  creds <- eitherError $ eitherDecode <$> BL.readFile (unpack $ xwikiCredentials wi)
  curl <- initialize
  _ <- setopt curl $ CurlSSLVerifyHost 2
  _ <- setopt curl $ CurlUserPwd $ unpack (xwikiUser creds) <> ":" <> unpack (xwikiPassword creds)
  _ <- setopt curl $ CurlHttpAuth [HttpAuthBasic]
  _ <- setopt curl $ CurlTCPNoDelay False
  _ <- setopt curl $ CurlFollowLocation True

  fst <$> runStateT (runCurlXWikiRest m) CurlXWikiRestState { cxrsWikiItem=wi, cxrsCurl=curl, cxrsCreate=False, cxrsFormat=JSON, cxrsPropertyValues = Map.empty }

getUrl' :: Text -> EntityReference -> Maybe Text -> CurlXWikiRest URLString
getUrl' extra entityRef translation = do
    CurlXWikiRestState s _ _ format _ <- get
    pure $ LT.unpack $ toLazyText $
      fromText (xwikiUrl s) <> fromText "/rest/" <> restUriFragment entityRef <> translations' <> fromText extra <> fromText (if format == JSON then "?media=json" else "")
  where
    translations' = case translation of
                      Just t -> fromText $ "/translations/" <> t
                      Nothing -> mempty

getUrl :: EntityReference -> Maybe Text -> CurlXWikiRest URLString
getUrl = getUrl' ""

getXClassUrl :: DocumentReference -> CurlXWikiRest URLString
getXClassUrl docRef = do
  wikiItem <- gets cxrsWikiItem
  pure $ LT.unpack $ toLazyText $
    fromText (xwikiUrl wikiItem) <> fromText "/rest/" <> xclassUriFragment docRef <> "?media=json"


doCurl' :: Text -> EntityReference -> Maybe Text -> [CurlOption] -> CurlXWikiRest (CurlResponse_ [(String, String)] BL.ByteString)
doCurl' extra  entityRef translation opts = do
  curl <- getCurl
  url <- getUrl' extra entityRef translation
  liftIO $ print url
  liftIO $ do_curl_ curl url opts

doCurl :: EntityReference -> Maybe Text -> [CurlOption] -> CurlXWikiRest (CurlResponse_ [(String, String)] BL.ByteString)
doCurl = doCurl' ""

getData :: EntityReference -> Text -> BL.ByteString
getData (DocumentEntity _) c = encode $ Object (HashMap.singleton "content" (String c))
getData (PropertyEntity (PropertyReference _ prop)) c = encode $ Object (HashMap.singleton prop (String c))
getData _ _ = undefined

cast :: (Integral a, Num b) => a -> b
cast = fromInteger . toInteger

byteStringReader :: BL.ByteString -> IO ReadFunction
byteStringReader buf = reader' <$> newIORef 0
  where
    reader' :: IORef Int -> ReadFunction
    reader' offsetRef ptr width n _ = do
      offset <- readIORef offsetRef
      let end = min (toInteger offset + toInteger (width * n)) (toInteger $ BL.length buf)
      if toInteger offset > toInteger end then pure $ Just 0 else
        do
          let n' = cast end - cast offset
          mapM_ (writeByte ptr offset) [0 .. n' - 1 ]
          writeIORef offsetRef $ offset + cast n
          pure $ Just $ cast n'
    writeByte ptr offset i =
      Storable.pokeElemOff ptr (cast i) $ cast $ BL.index buf (i + cast offset)


fileHandleReader :: Handle -> ReadFunction
fileHandleReader h ptr width n _ = do
  buf <- B.hGet h $ cast (width * n)
  mapM_ (\i -> Storable.pokeElemOff ptr i (cast $ B.index buf i)) [0 .. B.length buf - 1]
  pure $ Just $ cast $ B.length buf

curlGetOpts :: [CurlOption]
curlGetOpts = [
  CurlUpload False,
    CurlPut False,
    CurlNoBody False,
    CurlHttpHeaders ["Accept: application/json"]
  ]


instance XWikiRest CurlXWikiRest where

  getEntityContent = getEntityContent'

  getEntity = ((.).(.) $ fmap (mapM eitherDecode =<<)) $ getEntityContent'

  putEntity = doEntity

  putEntityContent = doEntity'

  putXObj = doXObj
  
  getSpaceEntities (Left wikiref) =
    curlGetSpaceEntities (WikiEntity wikiref)
  getSpaceEntities (Right spaceref) =
    curlGetSpaceEntities (SpaceEntity spaceref)

  getXClass docRef = do
    curl <- getCurl
    url <- getXClassUrl docRef
    liftIO $ print ("url: " <> url)
    resp <- liftIO $ do_curl_ curl url curlGetOpts
    case resp :: (CurlResponse_ [(String, String)] BL.ByteString) of
      CurlResponse { respCurlCode = code,
                     respBody     = body,
                     respStatus   = status,
                     respStatusLine = statusLine } -> do
        when (status /= 200 || code /= CurlOK) $ liftIO $
          print ("code: " <>  show code <> " status: " <> show status <> " statusline: " <> statusLine)
        pure $ eitherDecode body

  createNonExisting c = get >>= \s -> put s { cxrsCreate = c }

  pullXarWithSpaces spaces pullBackup = do
    CurlXWikiRestState wikiItem curl _ _ _ <- get
    (filename, wiki) <- case xwikiWiki wikiItem of
      Nothing -> fail "No wiki!"
      Just w -> pure (w <> ".xar", w)
    h <- liftIO $ openFile (unpack filename) WriteMode
    liftIO $ hSetBuffering h $ BlockBuffering Nothing

    let excluded = LT.toStrict .
                    toLazyText .
                    serializeSpaceReference .
                    (\s ->  SpaceReference (WikiReference wiki) [s]) <$>
                   (if pullBackup then xwikiBackupExcludeSpaces else xwikiDeployExcludeSpaces) wikiItem


    let spaces' = spaces \\ excluded

    let pages = fmap (("pages",) . Just . (<> ".%")) spaces' :: QueryText
    let curlOpts = [
          CurlUpload False,
          CurlPut False
          ]
    let url = unpack $ xwikiUrl wikiItem <> "/export/Space/Page" <> toStrict (decodeUtf8 $ toLazyByteString $ renderQueryText True (("format", Just "xar"):pages))

    liftIO $ putStrLn $ "Requesting " <> url
    
    resp <- liftIO (do_curl_ curl url curlOpts :: IO (CurlResponse_ [(String, String)] [B.ByteString] ))

    let CurlResponse { respCurlCode = code,
                       respStatus   = status,
                       respStatusLine = statusLine,
                       respBody = body
                       } = resp

    when (status /= 200 || code /= CurlOK) $ liftIO $
       print ("code: " <>  show code <> " status: " <> show status <> " statusline: " <> statusLine)

    liftIO $ mapM_ (B.hPut h) body
    liftIO $ hFlush h
    liftIO $ hClose h
    pure ()

  getPropertyValues xclass propertyName = do
    propertyValues <- gets cxrsPropertyValues
    case Map.lookup (xclassName xclass, propertyName) propertyValues of
      Just pvs -> pure $ Right pvs
      Nothing -> do
        wikiItem <- gets cxrsWikiItem
        curl <- gets cxrsCurl
        let wiki = fromMaybe "xwiki" $ xwikiWiki wikiItem
        let url = xwikiUrl wikiItem <> "/rest/wikis/" <> wiki <> "/classes/" <> xclassName xclass <> "/properties/" <> propertyName <> "/values?media=json"
        resp <- liftIO $ do_curl_ curl (unpack url) curlGetOpts
        case resp :: (CurlResponse_ [(String, String)] BL.ByteString) of
          CurlResponse { respCurlCode = code,
                         respStatus   = status,
                         respStatusLine = statusLine,
                         respBody = body } -> do
            when (status /= 200 || code /= CurlOK) $ liftIO $ print $ "getPropertyValues" <> url
            liftIO $ print ("code: " <>  show code <> " status: " <> show status <> " statusline: " <> statusLine)
            case eitherDecode body of
              Right pvs -> do
                modify (\s -> s { cxrsPropertyValues = Map.insert (xclassName xclass, propertyName) pvs propertyValues })
                pure $ Right pvs
              e         -> pure e


  setFormat format = get >>= \s -> put s { cxrsFormat = format }

  getFormat = get <&> \(CurlXWikiRestState _ _ _ format _) -> format


getEntityContent' :: Maybe Text -> EntityReference -> CurlXWikiRest (Either String (Maybe BL.ByteString))
getEntityContent' translation entityRef = do

  format <- getFormat
  
  let opts =
        CurlHttpHeaders ["Accept: " <> case format of
                            JSON  -> "application/json"
                            XML   -> "application/xml"
                            OCTET -> "application/octet-stream"] :
        [
        CurlUpload False,
        CurlPut False,
        CurlNoBody False
        ]

  resp <- doCurl entityRef translation opts
                    
  let CurlResponse { respCurlCode = code,
                     respStatus   = status,
                     respStatusLine = statusLine } = resp
  when (status /= 200 || code /= CurlOK) $ liftIO $ do
    print $ "getEntity " <> show entityRef
    print ("code: " <>  show code <> " status: " <> show status <> " statusline: " <> statusLine)

  pure $ if status == 404
         then Right Nothing
         else Right (Just $ respBody resp)


curlGetSpaceEntities :: EntityReference -> CurlXWikiRest (Either String [XWikiRestSpace])
curlGetSpaceEntities entity = do
  resp <- doCurl' "/spaces" entity Nothing curlGetOpts
  let CurlResponse { respCurlCode = code,
                     respStatus   = status,
                     respStatusLine = statusLine } = resp
  when (status /= 200 || code /= CurlOK) $ liftIO $ do
    print $ "getSpaceEntities " <> show entity
    print ("code: " <>  show code <> " status: " <> show status <> " statusline: " <> statusLine)

  pure $ fmap (\(XWikiRestSpaces _ spaces) -> spaces) <$> eitherDecode $ respBody resp

check :: (Monad m, Show e) => Either e a -> m a
check = check'
  where
    check' (Left e) = fail $ show e
    check' (Right v) = pure v

parseObjectReferenceJSON :: Maybe Text -> Value -> Parser ObjectReference
parseObjectReferenceJSON mwiki = withObject "body" $ \o -> ObjectReference <$>
    (o .: "className" >>= withText "className" (takeDocRef "className" . pe)) <*>
    (o .: "pageId" >>= withText "pageId" (takeDocRef "pageId" . pe)) <*>
    optional (o .: "number" >>= withScientific  "object number" (pure . floor))
  
    where
      takeDocRef msg (Left e) = fail (msg <> ": " <> show e)
      takeDocRef _ (Right (DocumentEntity docRef)) = pure docRef
      takeDocRef msg (Right _) = fail (msg <> ": " <> "expected document entity")

      pe = parseEntity' $ maybe defaultEntityParserState (\wiki -> defaultEntityParserState { psWiki = wiki }) mwiki

doXObj :: XObj -> ObjectReference -> CurlXWikiRest EntityReference
doXObj xobj objectRef@(ObjectReference classRef docRef mn) = do
  xclass <- getXClass classRef >>= check
  formdata <- xobj2formdata xclass xobj >>= check

  resp <- if isJust mn
          then doPut formdata
          else doPost formdata

  let CurlResponse { respCurlCode = code,
                     respStatus   = status,
                     respStatusLine = statusLine,
                     respBody = body } = resp

  WikiItem { xwikiWiki = wikiName } <- gets cxrsWikiItem

  if code == CurlOK && status == 404
    then (do
             _ <- putEntity "" Nothing (DocumentEntity docRef)
             doXObj xobj objectRef
             )
    else  if status `div` 100 == 2 && code == CurlOK
          then ObjectEntity <$> check (do
                                          v <- eitherDecode body
                                          parseEither (parseObjectReferenceJSON wikiName) v)
          else fail ("code: " <>  show code <> " status: " <> show status <> " statusline: " <> statusLine)

  where
    doPut formdata = do
      (buf, reader) <- formDataReader formdata
      
      doCurl (ObjectEntity objectRef) Nothing [CurlReadFunction reader,
                                               CurlInFileSize $ cast $ BL.length buf,
                                               CurlNoBody False,
                                               CurlUpload True,
                                               CurlHttpHeaders ["Content-Type: application/x-www-form-urlencoded; charset=utf-8",
                                                                "Accept: application/json"]]
    doPost formdata = do
      setFormat OCTET
      (buf, reader) <- formDataReader formdata
      resp <- doCurl (ObjectEntity objectRef) Nothing [ CurlPost True,
                                                        CurlReadFunction reader,
                                                        CurlNoBody False,
                                                        CurlPostFieldSize $ cast $ BL.length buf,
                                                        CurlHttpHeaders ["Content-Type: application/x-www-form-urlencoded; charset=utf-8",
                                                                         "Accept: application/json",
                                                                         "Expect: "]]
      setFormat JSON
      pure resp
      
    formDataReader formdata = do
      let buf = serializeFormdata formdata
      liftIO $ byteStringReader buf >>= \reader -> pure (buf, reader)


doEntity :: Text -> Maybe Text -> EntityReference -> CurlXWikiRest EntityReference
doEntity = doEntity' . encodeUtf8 . LT.fromStrict

doEntity' :: BL.ByteString -> Maybe Text -> EntityReference -> CurlXWikiRest EntityReference
doEntity' buf' _ (ObjectEntity objectRef) = do
  xobj <- check $ eitherDecode buf'
  doXObj xobj objectRef

doEntity' buf translation entityRef = do
  reader <- liftIO $ byteStringReader buf
  resp <- doCurl entityRef translation [CurlReadFunction reader,
                                        CurlInFileSize $ cast $ BL.length buf,
                                        CurlNoBody False,
                                        CurlUpload True,
                                        CurlHttpHeaders ["Content-type: " <> if isAttachment entityRef then "application/octet-stream" else "text/plain"]]
  case resp of
    CurlResponse { respCurlCode = code,
                   respStatus   = status,
                   respStatusLine = statusLine } -> when (status /= 200 || code /= CurlOK) $ liftIO $ do
      print $ "doEntity " <> show entityRef
      print ("code: " <>  show code <> " status: " <> show status <> " statusline: " <> statusLine)
  pure entityRef

  where
    isAttachment :: EntityReference -> Bool
    isAttachment (AttachmentEntity _) = True
    isAttachment _                    = False

getCurl :: CurlXWikiRest Curl
getCurl = gets cxrsCurl
