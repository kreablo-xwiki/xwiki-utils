{-# LANGUAGE RankNTypes #-}
module XWiki.Rest.Class(
  XWikiRest(..)
)
where

import Control.Monad.IO.Class
import Data.Aeson                (FromJSON)
import XWiki.Model
import XWiki.Rest.Space
import XWiki.Rest.Format
import Prelude                   (String, Bool)
import Data.Either
import Data.Text                 (Text)
import Data.Maybe
import XWiki.XClass              (XClass, XPropertyValues)
import XWiki.XObj                (XObj)
import Data.ByteString.Lazy

class (MonadIO r) => XWikiRest r where

  getEntity :: forall a. FromJSON a => Maybe Text -> EntityReference -> r (Either String (Maybe a))

  putEntity :: Text -> Maybe Text -> EntityReference -> r EntityReference

  getEntityContent :: Maybe Text -> EntityReference -> r (Either String (Maybe ByteString))

  putEntityContent :: ByteString -> Maybe Text -> EntityReference -> r EntityReference

  putXObj :: XObj -> ObjectReference -> r EntityReference

  getXClass :: DocumentReference -> r (Either String XClass)

  getPropertyValues :: XClass -> Text -> r (Either String XPropertyValues)

  getSpaceEntities :: Either WikiReference SpaceReference -> r (Either String [XWikiRestSpace])

  pullXarWithSpaces :: [Text] -> Bool -> r ()

  createNonExisting :: Bool -> r ()

  getFormat :: r Format

  setFormat :: Format -> r ()
  


