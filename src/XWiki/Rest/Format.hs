module XWiki.Rest.Format(
  Format(..)
) where

data Format = JSON | XML | OCTET deriving (Show, Eq)
