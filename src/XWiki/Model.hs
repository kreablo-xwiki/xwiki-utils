{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE LambdaCase #-}

module XWiki.Model (
#ifdef TEST
  escape,
  takeCharacters1,
  wikiReference,
  spaceReference,
  documentReference,
  entityReference,
#endif
  WikiReference(..),
  SpaceReference(..),
  DocumentReference(..),
  ObjectReference(..),
  PropertyReference(..),
  EntityReference(..),
  AttachmentReference(..),
  serializeEntityReference,
  serializeAttachmentReference,
  serializePropertyReference,
  serializeObjectReference,
  serializeDocumentReference,
  serializeSpaceReference,
  serializeWikiReference,
  parseEntity,
  parseEntity',
  EntityParserState(..),
  defaultEntityParserState,
  restUriFragment,
  eitherError,
  entityContent,
  xclassUriFragment,
  documentReferenceToSpaceReference,
  setWiki,
  entityFilepath
) where

import Data.Text (Text, pack)
import Data.Text.Lazy (toStrict)
import Data.Text.Lazy.Encoding (decodeUtf8)
import Data.Text.Encoding (encodeUtf8)
import Data.Text.Lazy.Builder
import Data.Text.Lazy.Builder.Int
import Data.Char
import Data.Eq
import Data.Monoid
import qualified Data.List as L
import Data.Foldable
import Data.ByteString.Builder(toLazyByteString)
import qualified Data.ByteString.Char8 as B
import qualified Data.Set as Set
import Text.Parsec        (satisfy, anyChar, many1, char, optionMaybe, digit, try, runParser, ParseError, modifyState, getState, parserZero)
import Text.Parsec.Text   (GenParser)
import Data.Functor
import Data.Maybe
import Data.Either
import Control.Applicative
import Control.Monad
import Prelude(($), (.), (-), (==), Int, read, Show, String, show, Bool(..))
import qualified Data.HashMap.Lazy as HashMap
import Data.Aeson
import Network.HTTP.Types.URI(encodePathSegmentsRelative, urlEncode)
import System.FilePath (FilePath)

newtype WikiReference = WikiReference Text deriving (Eq, Show)

newtype EntityParserState = EntityParserState {
  psWiki :: Text
} deriving (Show, Eq)

type Parser a = GenParser EntityParserState a

defaultEntityParserState :: EntityParserState
defaultEntityParserState = EntityParserState {
  psWiki = "xwiki"
}

escape :: Parser Char
escape = satisfy (== '\\') >> anyChar

takeCharacters1 :: String -> Parser Text
takeCharacters1 chars = pack <$>
  let
    set = Set.fromList $ '\\' : chars
  in
    many1 $ satisfy (`Set.notMember` set) <|> escape

wikiReference :: Parser WikiReference
wikiReference = WikiReference <$> do
  wiki <- takeCharacters1 ":"
  modifyState (\s -> s { psWiki = wiki })
  pure wiki

data SpaceReference = SpaceReference WikiReference [Text] deriving (Eq, Show)

spaceReference :: Parser SpaceReference
spaceReference = SpaceReference <$> (try (do
  wr <- wikiReference
  _ <- char ':'
  pure wr) <|> (getState >>= \s -> pure (WikiReference $ psWiki s))) <*>
    (many (try $ do
        sr' <- spaceReference'
        _ <- char '.'
        pure sr') <|> pure [])
  where
    spaceReference' :: Parser Text
    spaceReference' = takeCharacters1 ".^@[]"

data DocumentReference = DocumentReference SpaceReference Text deriving (Eq, Show)

data AttachmentReference = AttachmentReference DocumentReference Text deriving (Eq, Show)

{-
attachmentReference :: Parser AttachmentReference
attachmentReference = try (AttachmentReference <$> documentReference <*> (char '@' >> takeCharacters1 ""))
-}

data EntityReference =
  WikiEntity WikiReference |
  SpaceEntity SpaceReference |
  DocumentEntity DocumentReference   |
  ObjectEntity ObjectReference       |
  PropertyEntity PropertyReference   |
  AttachmentEntity AttachmentReference deriving (Eq, Show)

documentReferenceToSpaceReference :: DocumentReference -> SpaceReference
documentReferenceToSpaceReference (DocumentReference (SpaceReference wikiRef spaces) text) = SpaceReference wikiRef (spaces <> [text])

documentReference :: Parser DocumentReference
documentReference = DocumentReference <$> spaceReference <*> takeCharacters1 "@^[]"


serializeEntityReference' :: Bool -> EntityReference -> Builder
serializeEntityReference' withWiki entityRef =
  case entityRef of
    WikiEntity wikiRef     -> serializeWikiReference wikiRef
    SpaceEntity spaceRef   -> serializeSpaceReference' withWiki spaceRef
    DocumentEntity docRef  -> serializeDocumentReference' withWiki docRef
    ObjectEntity objRef    -> serializeObjectReference' withWiki objRef
    PropertyEntity propRef -> serializePropertyReference' withWiki propRef
    AttachmentEntity attRef -> serializeAttachmentReference' withWiki attRef


serializeAttachmentReference :: AttachmentReference -> Builder
serializeAttachmentReference = serializeAttachmentReference' True

serializeAttachmentReference' :: Bool -> AttachmentReference -> Builder
serializeAttachmentReference' withWiki (AttachmentReference docRef filename) = serializeDocumentReference' withWiki docRef <> singleton '@' <> fromText filename

serializeEntityReference :: EntityReference -> Builder
serializeEntityReference = serializeEntityReference' True

serializePropertyReference' :: Bool -> PropertyReference -> Builder
serializePropertyReference' withWiki (PropertyReference objRef text) = serializeObjectReference' withWiki objRef <> singleton '.' <> fromText text

serializePropertyReference :: PropertyReference -> Builder
serializePropertyReference = serializePropertyReference' True

serializeObjectReference' :: Bool -> ObjectReference -> Builder
serializeObjectReference' withWiki (ObjectReference classRef docRef mn) =
  let obj' = serializeDocumentReference' withWiki docRef <> singleton '^' <>
             serializeDocumentReference' False classRef
  in
    case mn of
      Just n  -> obj' <> singleton '[' <> decimal n <> singleton ']'
      Nothing -> obj'

serializeObjectReference :: ObjectReference -> Builder
serializeObjectReference = serializeObjectReference' True

serializeDocumentReference' :: Bool -> DocumentReference -> Builder
serializeDocumentReference' withWiki (DocumentReference spaceRef text) = serializeSpaceReference' withWiki spaceRef <> singleton '.' <> fromText text

serializeDocumentReference :: DocumentReference -> Builder
serializeDocumentReference = serializeDocumentReference' True

serializeSpaceReference' :: Bool -> SpaceReference -> Builder
serializeSpaceReference' withWiki (SpaceReference wikiRef spaces) = (if withWiki then serializeWikiReference wikiRef <> singleton ':' else mempty) <> serializeSpaces spaces
  where
    serializeSpaces :: [Text] -> Builder
    serializeSpaces [] = mempty
    serializeSpaces [s] = fromText s
    serializeSpaces (s:ss) = fromText s <> singleton '.' <> serializeSpaces ss

serializeSpaceReference :: SpaceReference -> Builder
serializeSpaceReference = serializeSpaceReference' True

serializeWikiReference :: WikiReference -> Builder
serializeWikiReference (WikiReference text) = fromText text

fpenc :: Text -> FilePath
fpenc = B.unpack . urlEncode False . encodeUtf8

entityFilepath :: EntityReference -> [FilePath]
entityFilepath entityRef =
  case entityRef of
    WikiEntity wikiRef     -> wikiReferenceFilepath wikiRef
    SpaceEntity spaceRef   -> spaceReferenceFilepath spaceRef
    DocumentEntity docRef  -> documentReferenceFilepath docRef
    ObjectEntity objRef    -> objectReferenceFilepath objRef
    PropertyEntity propRef -> propertyReferenceFilepath propRef
    AttachmentEntity attRef -> attachmentReferenceFilepath attRef

wikiReferenceFilepath :: WikiReference -> [FilePath]
wikiReferenceFilepath (WikiReference text) = [fpenc text]

spaceReferenceFilepath :: SpaceReference -> [FilePath]
spaceReferenceFilepath (SpaceReference wikiRef spaces) = wikiReferenceFilepath wikiRef <> (fpenc <$> spaces)

documentReferenceFilepath' :: DocumentReference -> [FilePath]
documentReferenceFilepath' (DocumentReference spaceRef text) = spaceReferenceFilepath spaceRef <> [fpenc text]


documentReferenceFilepath :: DocumentReference -> [FilePath]
documentReferenceFilepath docRef = documentReferenceFilepath' docRef <> ["content.txt"]

objectReferenceFilepath :: ObjectReference -> [FilePath]
objectReferenceFilepath (ObjectReference classRef docRef mn) =
  documentReferenceFilepath' docRef <> ["objects"] <>
             [fpenc . toStrict . toLazyText $ serializeDocumentReference' False classRef <> num mn]

   where
     num (Just n) = singleton '[' <> decimal n <> singleton ']'
     num Nothing  = mempty

propertyReferenceFilepath :: PropertyReference -> [FilePath]
propertyReferenceFilepath (PropertyReference objRef text) =
  let
    ofp = objectReferenceFilepath objRef
    (fps, [obj]) = L.splitAt (length ofp - 1) ofp
  in
    fps <> [obj <> fpenc ("." <> text)]

attachmentReferenceFilepath :: AttachmentReference -> [FilePath]
attachmentReferenceFilepath (AttachmentReference docRef filename) = documentReferenceFilepath' docRef <> ["attachments", fpenc filename]

data ObjectReference = ObjectReference {
  objRefClass    :: DocumentReference,
  objRefDocument :: DocumentReference,
  objRefNumber   :: Maybe Int
   } deriving (Eq, Show)

data PropertyReference = PropertyReference ObjectReference Text deriving (Eq, Show)

entityReference :: Parser EntityReference
entityReference = do
  dr@(DocumentReference (SpaceReference (WikiReference wikiName) _) _) <- documentReference
  try (attachmentEntity dr) <|> try (objectEntity dr wikiName) <|> pure (DocumentEntity dr)

  where

    objectEntity :: DocumentReference -> Text -> Parser EntityReference
    objectEntity dr wikiName = do
      w' <- psWiki <$> getState
      modifyState (\s -> s { psWiki = wikiName })
      mo <- optionMaybe objectReference
      modifyState (\s -> s { psWiki = w' })
      case mo of
        Just (cr, n, Just p)  -> pure $ PropertyEntity $ PropertyReference ObjectReference { objRefClass = cr, objRefDocument = dr, objRefNumber = n } p
        Just (cr, n, Nothing) -> pure $ ObjectEntity ObjectReference { objRefClass = cr, objRefDocument = dr, objRefNumber = n }
        _                     -> parserZero

    attachmentEntity :: DocumentReference -> Parser EntityReference
    attachmentEntity dr = AttachmentEntity . AttachmentReference dr <$> (char '@' >> takeCharacters1 "")

    objectReference :: Parser (DocumentReference, Maybe Int, Maybe Text)
    objectReference = do
      _ <- char '^'
      cr <- documentReference
      mn <- optionMaybe $ try $ do
        _ <- char '['
        n' <- many1 digit
        _ <- char ']'
        pure $ read n'
      mpr <- optionMaybe $ try propertyReference
      pure (cr, mn, mpr)

    propertyReference :: Parser Text
    propertyReference = pack <$> do
      _ <- char '.'
      many1 anyChar

parseEntity :: Text -> Either ParseError EntityReference
parseEntity = parseEntity' defaultEntityParserState

parseEntity' :: EntityParserState -> Text -> Either ParseError EntityReference
parseEntity' state = runParser entityReference state "entityreference"

restUriFragment' :: EntityReference -> [Text]
restUriFragment' (WikiEntity (WikiReference wikiRef)) =
  ["wikis",  wikiRef]
restUriFragment' (SpaceEntity spaceRef) =
  spaceUriFragment spaceRef
  where
    spaceUriFragment :: SpaceReference -> [Text]
    spaceUriFragment (SpaceReference wikiRef spaces) =
      restUriFragment' (WikiEntity wikiRef) <> spacesUriFragment spaces
    spacesUriFragment :: [Text] -> [Text]
    spacesUriFragment = foldr (\s -> (<>) ["spaces", s]) mempty
restUriFragment'  (DocumentEntity (DocumentReference spaceRef text)) =
  restUriFragment' (SpaceEntity spaceRef) <> ["pages", text]
restUriFragment'  (ObjectEntity ObjectReference { objRefClass = className, objRefDocument = docRef, objRefNumber = mn }) = restUriFragment' (DocumentEntity docRef) <> ["objects"] <> maybe [] (\n -> [toStrict $ toLazyText $ serializeDocumentReference' False className, toStrict $ toLazyText $ decimal n]) mn
restUriFragment'  (PropertyEntity (PropertyReference objRef prop)) = restUriFragment' (ObjectEntity objRef) <> ["properties",  prop]
restUriFragment'  (AttachmentEntity (AttachmentReference docRef filename)) = restUriFragment' (DocumentEntity docRef) <> ["attachments", filename]

restUriFragment :: EntityReference -> Builder
restUriFragment = fromLazyText . decodeUtf8 . toLazyByteString . encodePathSegmentsRelative . restUriFragment'

xclassUriFragment :: DocumentReference -> Builder
xclassUriFragment docRef@(DocumentReference (SpaceReference wikiRef _) _) =
  fromLazyText (decodeUtf8 $ toLazyByteString $ encodePathSegmentsRelative $ restUriFragment' (WikiEntity wikiRef) <> ["classes"])
  <> "/" <> serializeDocumentReference docRef

eitherError :: (Monad m, Show e) => m (Either e a) -> m a
eitherError r = r >>= \case
    Left error -> fail $ show error
    Right x    -> pure x

entityContent :: EntityReference -> Value -> Either Text Text
entityContent (DocumentEntity _) (Object o) = case HashMap.lookup "content" o of
  Just (String t)  -> Right t
  Just _           -> Left "JSON value had invalid content field."
  _                -> Left "JSON value had no content field."
entityContent (PropertyEntity _) (Object o) = case HashMap.lookup "value" o of
  Just (String t)  -> Right t
  Just _           -> Left "JSON value had invalid value field."
  _                -> Left "JSON value had no value field."
entityContent _ _ = Left "Unsupported entity."

setWiki :: EntityReference -> Text -> EntityReference
setWiki (WikiEntity _) = WikiEntity . WikiReference
setWiki (SpaceEntity spaceRef) = SpaceEntity . setSpaceWiki spaceRef
setWiki (DocumentEntity docRef) = DocumentEntity . setDocWiki docRef
setWiki (ObjectEntity objRef) = ObjectEntity . setObjWiki objRef
setWiki (PropertyEntity propRef) = PropertyEntity . setPropWiki propRef
setWiki (AttachmentEntity attRef) = AttachmentEntity . setAttWiki attRef

setSpaceWiki :: SpaceReference -> Text -> SpaceReference             
setSpaceWiki (SpaceReference _ spaces) wiki = SpaceReference (WikiReference wiki) spaces
             
setDocWiki :: DocumentReference -> Text -> DocumentReference
setDocWiki (DocumentReference spaceRef text) wiki = DocumentReference (setSpaceWiki spaceRef wiki) text

setObjWiki :: ObjectReference -> Text -> ObjectReference
setObjWiki (ObjectReference classRef docRef mn) wiki = ObjectReference (setDocWiki classRef wiki) (setDocWiki docRef wiki) mn

setPropWiki :: PropertyReference -> Text -> PropertyReference
setPropWiki (PropertyReference objRef text) wiki = PropertyReference (setObjWiki objRef wiki) text

setAttWiki :: AttachmentReference -> Text -> AttachmentReference
setAttWiki (AttachmentReference docRef filename) wiki = AttachmentReference (setDocWiki docRef wiki) filename
