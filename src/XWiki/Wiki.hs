{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
module XWiki.Wiki(
  WikiItem(..),
  WikiCredentials(..),
  Entity(..)
) where

import Data.Text(Text)
import Data.Text.Lazy.Builder (toLazyText)
import XWiki.Model
import Data.Aeson
import Data.Aeson.Types (Parser)
import Data.Either
import Data.Maybe
import Data.Monoid
import Prelude(Show(..), ($), Eq, null)
import Control.Applicative
import Control.Monad
import Text.Parsec(ParseError)
import System.IO(FilePath)

data Entity = Entity (Maybe Text) FilePath EntityReference deriving (Show, Eq)

instance FromJSON (Maybe Text -> Either ParseError Entity) where
  parseJSON = withObject "Entity" $ \o ->
      (o .: "entity") >>= entityRef >>= entity (Entity <$> (o .:? "translation") <*> (o .: "file"))

    where
      entityRef :: Value -> Parser (Maybe Text -> Either ParseError EntityReference)
      entityRef (String s) = pure $ \mwiki -> parseEntity' (state mwiki) s
      entityRef _ = fail "Expected string"

      state :: Maybe Text -> EntityParserState
      state Nothing     = defaultEntityParserState
      state (Just wiki) = defaultEntityParserState { psWiki = wiki}

      entity :: Parser (EntityReference -> Entity) -> (Maybe Text -> Either ParseError EntityReference) -> Parser (Maybe Text -> Either ParseError Entity)
      entity p e = combine <$> p <*> pure e
        where
          combine :: (EntityReference -> Entity) -> (Maybe Text -> Either ParseError EntityReference) -> Maybe Text -> Either ParseError Entity
          combine f g mt = f <$> g mt


instance ToJSON Entity where
  toJSON (Entity mtranslation filepath entityRef) = object $ t mtranslation [ "entity" .= toLazyText (serializeEntityReference entityRef), "file" .= filepath ]
    where
      t (Just translation) a = ("translation" .= translation) : a
      t _ a = a

data WikiItem = WikiItem {
  xwikiUrl  :: Text,
  xwikiWiki :: Maybe Text,
  xwikiCredentials :: Text,
  xwikiBackupExcludeSpaces :: [Text],
  xwikiDeployExcludeSpaces :: [Text],
  xwikiEntities :: [Entity]
} deriving (Show, Eq)


data WikiCredentials = WikiCredentials {
  xwikiUser :: Text,
  xwikiPassword :: Text
}

data WikiItemWrapper = WikiItemWrapper WikiItem [Maybe Text -> Either ParseError Entity]

instance FromJSON WikiItemWrapper where

  parseJSON = withObject "WikiItem" $ \o -> WikiItemWrapper <$> (WikiItem <$>
    o .: "url" <*>
    o .:? "wiki" <*>
    o .: "credentials" <*>
    o .:? "backupExcludeSpaces" .!= [] <*>
    o .:? "deployExcludeSpaces" .!= [] <*>
    pure []) <*>
    o .:? "entities" .!= []

instance FromJSON WikiItem where
  parseJSON v = parseJSON v >>= \(WikiItemWrapper wikiItem unresolvedEntities) -> do
    let eitherEntities = ($ xwikiWiki wikiItem) <$> unresolvedEntities
    let errors = lefts eitherEntities
    let resolvedEntities = rights eitherEntities
    unless (null errors) $ fail $ "There where invalid entities in the JSON file: " <> show errors
    pure $ wikiItem { xwikiEntities = resolvedEntities }

instance ToJSON WikiItem where
  toJSON (WikiItem url mwiki credentials backupExcludeSpaces deployExcludeSpaces entities) =
    object $ a mwiki ["url" .= url,
                      "credentials" .= credentials,
                      "backupExcludeSpaces" .= backupExcludeSpaces,
                      "deployExcludeSpaces" .= deployExcludeSpaces,
                      "entities" .= entities]

    where
      a (Just wiki) o = ("wiki" .= wiki) : o
      a _ o = o

instance FromJSON WikiCredentials where

  parseJSON = withObject "Credentials" $ \o -> WikiCredentials <$>
    o .: "user" <*>
    o .: "password"

instance ToJSON WikiCredentials where
  toJSON (WikiCredentials user password) = object ["user" .= user, "password" .= password]
