module XWiki.Content (
  XWikiContent(..)
)
where



class XWikiContent a where

  content :: a -> ByteString
